//Sticky navbar
window.onscroll = function() {stickNavbar()};

var navbar = document.getElementById("navbar-wrapper");

var sticky = navbar.offsetTop;

function stickNavbar() {
  if (window.pageYOffset >= sticky) {
    navbar.classList.add("sticky")
  } else {
    navbar.classList.remove("sticky");
  }
}

//Navbar activation
var logo = document.getElementById("logo");
var miksi = document.getElementById("miksi");
var mita = document.getElementById("mita");
var kenelle = document.getElementById("kenelle");
var kouluttaja = document.getElementById("kouluttaja");
var ilmoittaudu = document.getElementById("ilmoittaudu");


//Navbar navigation
function moveToTop() {
    logo.scrollIntoView({ behavior: 'smooth', block: 'center' });
}

function moveToMiksi() {
    miksi.scrollIntoView({ behavior: 'smooth', block: 'center' });
}

function moveToMita() {
    mita.scrollIntoView({ behavior: 'smooth', block: 'center' });
}

function moveToKenelle() {
    kenelle.scrollIntoView({ behavior: 'smooth', block: 'center' });
}

function moveToKouluttaja() {
    kouluttaja.scrollIntoView({ behavior: 'smooth', block: 'center' });
}

function moveToIlmoittaudu() {
    ilmoittaudu.scrollIntoView({ behavior: 'smooth', block: 'center' });
}